// By: Hamza Mustapha 
// Game manager - IEnum StartupManager calls the api when the application start.
// In the application the GameObject (GameManager) will make this call and then connects the APIscript 


using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
[RequireComponent(typeof(SensorManager))]   
 
public class Managers : MonoBehaviour {
   public static SensorManager Sensor {get; private set;}
   public static ApiScript api {get; private set;}

   [SerializeField]
   private UIServerResponseViewer console;
   
   private List<IGameManager> _startSequence;
   
   void Awake() {
      Sensor = GetComponent<SensorManager>();

      _startSequence = new List<IGameManager>();
      _startSequence.Add(Sensor);

      StartCoroutine(StartupManagers());
   }


   private IEnumerator StartupManagers() {
      Debug.Log("Connecting to SMT API");
      //console.ShowMessage(string.Format("Establishing Connection.."));
      NetworkService network = new NetworkService();    
       foreach (IGameManager manager in _startSequence) {
         manager.Startup(network);   
      }

      yield return null;

      int numModules = _startSequence.Count;
      int numReady = 0;

      while (numReady < numModules) {
         int lastReady = numReady;
         numReady = 0;

         foreach (IGameManager manager in _startSequence) {
            if (manager.status == ManagerStatus.Started) {
               numReady++;
            }
         }

         if (numReady > lastReady)
            Debug.Log("Progress: " + numReady + "/" + numModules);

         yield return null;
      }
      console.ClearMessage();
      Debug.Log("Start Up Completed");
      console.ShowMessage(string.Format("API Connection Established"));
   }


// Example of another way to call the node from the smt api instead of through the apiscript 
   private IEnumerator StartupNodes() {
      NetworkNodes networkTwo = new NetworkNodes();
      foreach (IGameManager manager in _startSequence) {
         manager.StartNode(networkTwo);
      }

      yield return null;
      
      int numModules = _startSequence.Count;
      int numReady = 0;

      while (numReady < numModules) {
         int lastReady = numReady;
         numReady = 0;

         foreach (IGameManager manager in _startSequence) {
            if (manager.status == ManagerStatus.Started) {
               numReady++;
            }
         }

         if (numReady > lastReady)
            Debug.Log("Progress: " + numReady + "/" + numModules);

         yield return null;
      }
       Debug.Log("Nodes have been Loaded");

   }

}
// Require the new manager instead of player and inventory.
// Instantiate NetworkService to inject in all managers
// Pass the network service to managers during startup

