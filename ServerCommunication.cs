// By: Hamza Mustapha 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Xml;
using System.IO;

/// <summary>
/// This class is responsible for handling REST API requests to remote server.
/// To extend this class you just need to add new API methods.
/// </summary>
public class ServerCommunication : PersistentLazySingleton<ServerCommunication>
{
    #region [Server Communication]
    [SerializeField]

    /// <summary>
    /// This method is used to begin sending request process.
    /// </summary>
    /// <param name="url">API url.</param>
    /// <param name="callbackOnSuccess">Callback on success.</param>
    /// <param name="callbackOnFail">Callback on fail.</param>
    /// <typeparam name="T">Data Model Type.</typeparam>
    private void SendRequest<T>(string url, UnityAction<T> callbackOnSuccess, UnityAction<string> callbackOnFail)
    {
        StartCoroutine(RequestCoroutine(url, callbackOnSuccess, callbackOnFail));
    }

    /// <summary>
    /// Coroutine that handles communication with REST server.
    /// </summary>
    /// <returns>The coroutine.</returns>
    /// <param name="url">API url.</param>
    /// <param name="callbackOnSuccess">Callback on success.</param>
    /// <param name="callbackOnFail">Callback on fail.</param>
    /// <typeparam name="T">Data Model Type.</typeparam>
    private IEnumerator RequestCoroutine<T>(string url, UnityAction<T> callbackOnSuccess, UnityAction<string> callbackOnFail)
    {

        using (UnityWebRequest request = UnityWebRequest.Get(url)) { 
        // request and wait for the page to load      
        yield return request.SendWebRequest();
        
        // Error 
        if (request.result !=  UnityWebRequest.Result.Success) {    
         Debug.LogError("Server Communication Error: " + request.result); 
        } else if (request.responseCode !=    
              (long)System.Net.HttpStatusCode.OK) {    
            Debug.LogError("response error: " + request.responseCode);
        } else {
             Debug.Log(request.downloadHandler.text);
             Debug.Log("Server Connected"); 
        }
      }
    }

    /// <summary>
    /// This method finishes request process as we have received answer from server.
    /// </summary>
    /// <param name="data">Data received from server in XML format.</param>
    /// <param name="callbackOnSuccess">Callback on success.</param>
    /// <param name="callbackOnFail">Callback on fail.</param>
    /// <typeparam name="T">Data Model Type.</typeparam>
    private void ParseResponse<T>(string data, UnityAction<T> callbackOnSuccess, UnityAction<string> callbackOnFail)
    {
        
    }

    #endregion

    #region [API]

    /// <summary>
    /// This method call server API to get LOGIN_URL.
    /// </summary>
    /// <param name="callbackOnSuccess">Callback on success.</param>
    /// <param name="callbackOnFail">Callback on fail.</param>
    public void LoginAPI(UnityAction<string> callbackOnSuccess, UnityAction<string> callbackOnFail)
    {
        SendRequest(string.Format(ServerConfig.LOGIN_URL), callbackOnSuccess, callbackOnFail);
    }

    /// <summary>
    /// This method call server API to get LOGOUT_URL.
    /// </summary>
    /// <param name="callbackOnSuccess">Callback on success.</param>
    /// <param name="callbackOnFail">Callback on fail.</param>
    public void LogoutAPI(UnityAction<string> callbackOnSuccess, UnityAction<string> callbackOnFail)
    {
        SendRequest(string.Format(ServerConfig.LOGOUT_URL), callbackOnSuccess, callbackOnFail);
    }

    /// <summary>
    /// This method call server API to get REQUEST_NODE_LIST_URL.
    /// </summary>
    /// <param name="callbackOnSuccess">Callback on success.</param>
    /// <param name="callbackOnFail">Callback on fail.</param>
    public void NodeAPI(UnityAction<string> callbackOnSuccess, UnityAction<string> callbackOnFail){
        SendRequest(string.Format(ServerConfig.REQUEST_NODE_LIST_URL), callbackOnSuccess, callbackOnSuccess);
    }

     /// <summary>
    /// This method call server API to get REQUEST_SENSOR_DATA_URL.
    /// The file UIServerSensorButton started this proccess.
    /// </summary>
    /// <param name="callbackOnSuccess">Callback on success.</param>
    /// <param name="callbackOnFail">Callback on fail.</param>
    public void SensorAPI(UnityAction<string> callbackOnSuccess, UnityAction<string> callbackOnFail){
        SendRequest(string.Format(ServerConfig.REQUEST_SENSOR_LIST_URL), callbackOnSuccess, callbackOnSuccess);
    }



    #endregion
}
