//By: Hamza Mustapha
//URL to send request to
//Create UnityWebRequest object in GET mode.
//Pause while downloading.
//Check for errors in the response.
//Delegate can be called just like the original function.
//Yield cascades through coroutine methods that call each other

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

// Login and logout 
public class NetworkService {
   string LOGIN_URL = "https://analytics.smtresearch.ca/api/?action=login&user_username=hamza&user_password=smt";
   
   private IEnumerator CallLoginAPI(string url, Action<string> callback) {

      using (UnityWebRequest request = UnityWebRequest.Get(url)) { 
      // request and wait for the page to load      
      yield return request.SendWebRequest();

      // Error 
      if (request.result !=  UnityWebRequest.Result.Success) {    
         Debug.LogError("Login problem: " + request.result); 
         } else if (request.responseCode !=    
              (long)System.Net.HttpStatusCode.OK) {    
            Debug.LogError("response error: " + request.responseCode);
         } else {
            callback(request.downloadHandler.text);    
         }
      }
   }
   public IEnumerator Login(Action<string> callback) {
      return CallLoginAPI(LOGIN_URL, callback); 
   }   
}


