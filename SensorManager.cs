// By: Hamza Mustapha 
// SensorManager connnects to the api and parses the XML. Its an alternative way from the APIscript code. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System;
using System.IO;
using UnityEngine.Networking;

public class SensorManager : MonoBehaviour, IGameManager {
   public ManagerStatus status {get; private set;}
   [SerializeField]
   private UIServerResponseViewer console;
   private NetworkService _network;
   private NetworkNodes _networkTwo;
   public Dictionary<string, string> headers;

   public List<SmtApiResponses.ApiNode> nodes;

   void Awake() {
        nodes = new List<SmtApiResponses.ApiNode>();
        //sensors = new List<SmtApiResponses.Sensor>();
        //readings = new List<SmtApiResponses.Reading>();
    }

   public void Startup(NetworkService service) {
      Debug.Log("Sensor manager starting...");
      _network = service;
      StartCoroutine(_network.Login(OnXMLDataLoaded));
      console.ClearMessage();
      console.ShowMessage(string.Format("API Connection Established"));

      status = ManagerStatus.Initializing;    
   }

   public void StartNode(NetworkNodes serviceTwo){
      Debug.Log("Nodes manager is starting...");
      _networkTwo = serviceTwo;
      StartCoroutine(_networkTwo.NodesList(OnNodeXMLData));

      status = ManagerStatus.Initializing; 
   }

   // Loads xml from LOGIN_URL (NetworkService)
   public void OnXMLDataLoaded(string data) {
      Debug.Log(data);
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(data);

      //Get API Xml and store the PHPSESSID for the session 
      XmlElement root = doc.DocumentElement;
      XmlNodeList elemList = root.GetElementsByTagName("PHPSESSID");
      for (int i=0; i < elemList.Count; i++)
      {
         string sessionID = elemList[i].InnerXml;
         Debug.Log(sessionID);

         WWWForm wwwform = new WWWForm();

         headers = wwwform.headers;
         headers.Add("Cookie", "PHPSESSID=" + sessionID);
      }
      status = ManagerStatus.Started;
   }

   public void OnNodeXMLData(string data){
      //Debug.Log("break");
      status = ManagerStatus.Started;
   } 
}

// Store the injected NetworkService object.

